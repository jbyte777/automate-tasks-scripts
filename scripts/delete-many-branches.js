var fs = require('fs');
var path = require('path');
var { exec } = require('child_process');

var argsFlags = {
  '-b': true,
};

var isBranchNameChar = (ch) => {
  return ch === '.'
    || ch === '-'
    || ch === '_'
    || ('0' <= ch && ch <= '9')
    || ('a' <= ch && ch <= 'z')
    || ('A' <= ch && ch <= 'Z');
};

var getDeleteBranchCommand = (branchesText, keepedBranches) => {
  const deletedBranches = [];

  let p = 0;
  while (p < branchesText.length) {
    if (isBranchNameChar(branchesText[p])) {
      const begin = p;
      while (p < branchesText.length && isBranchNameChar(branchesText[p])) {
        p++;
      }

      const name = branchesText.slice(begin, p);
      if (!keepedBranches[name]) {
        deletedBranches.push(name);
      }
      continue;
    }

    p++;
  }

  return 'git branch -d '.concat(
    deletedBranches.join(' '),
  );
};

var getParamsFromArgs = (args) => {
  const branches = {};

  let i = 0;
  while (i < args.length) {
    if (args[i] === '-b') {
      i++;
      while (i < args.length && !argsFlags[args[i]]) {
        branches[args[i]] = true;
        i++;
      }
      continue;
    }

    i++;
  }

  return {
    branches,
  };
};

var execCommand = async (command) => {
  return await new Promise((res, rej) => {
    exec(command, (err, stdout, stderr) => {
      if (err) {
        rej(err);
        return;
      }

      if (stderr) {
        rej(stderr);
        return;
      }

      res(stdout);
    });
  });
};

var main = async () => {
  const args = process.argv.slice(2);
  const { branches } = getParamsFromArgs(args);
  const branchesText = await execCommand('git branch');
  const deleteCommand = getDeleteBranchCommand(branchesText, branches);
  await execCommand(deleteCommand);
};

main().catch(err => console.error(err));
