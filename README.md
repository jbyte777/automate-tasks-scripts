# Automating scripts for developer

## `scripts/delete-many-branches.js`

Deletes all branches except defined in the command. Arguments for it are:
- `-b` - branches that are not deleted from a list of current branches;
